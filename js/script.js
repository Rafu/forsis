import $ from 'jquery';
import 'bootstrap';
import 'select2';
import '@fortawesome/fontawesome-free/js/all';
import '../scss/style.scss';
import ScrollReveal from 'scrollreveal'

$('[apply-select2]').select2({
  theme: 'bootstrap4',
  minimumResultsForSearch: -1,
  placeholder: 'Select an option',
  closeOnSelect: false
});

$(".more-btn, .more-info-btn").click(function () {
  $('.more-section').slideToggle("slow");
});


let $hamburger = $(".hamburger");
$hamburger.on("click", function (e) {
  $hamburger.toggleClass("is-active");
});

// $(window).scroll(function () {
//   if ($(this).scrollTop() >= 300) {
//     $('.navbar.navbar-expand-md').css('box-shadow', '0px 5px 10px rgba(0,0,0,.1)');
//     $('.navbar.navbar-expand-md').css('transition', '0.4');
//   }
//   if ($(this).scrollTop() == 0) {
//     $('.navbar.navbar-expand-md').css('box-shadow', 'none');
//     $('.navbar.navbar-expand-md').css('background-color', '#ffffff');

//   }
// });

$(window).scroll(function () {
  let scroll = $(window).scrollTop();

  if (scroll >= 100) {

    $("header").addClass("active");
    $("nav").addClass("scroll-nav");
    $(".navbar-brand .logo").addClass("d-none");
    $(".navbar-brand .logo-purple").removeClass("d-none");
    $(".navbar-brand .logo-purple").addClass("d-block");

  } else {
    $("header").removeClass("active");
    $("nav").removeClass("scroll-nav");
    $(".navbar-brand .logo").removeClass("d-none");
    $(".navbar-brand .logo-purple").removeClass("d-block");
    $(".navbar-brand .logo-purple").addClass("d-none");

  }
});


$( ".navbar-toggler" ).click(function() {
  $("nav").addClass("scroll-nav");
  $(".navbar-brand .logo").addClass("d-none");
  $(".navbar-brand .logo-purple").removeClass("d-none");
  $(".navbar-brand .logo-purple").addClass("d-block");
});




ScrollReveal().reveal(
  '.global-info .text-resume, .stepper, .multiple-button .multiple-button-resume, .stepper-form-uni, .resume, .button-section h2, .button-section button, .text-resume-1, .resume-trio .left-block, .resume-trio .center-block, .resume-trio .right-block, .card-4 .section-resume, .two-column-resume .resume-row, .two-column-resume button, .comment .quote-img, .comment .comment-text, .comment .portrait, .comment .identity', {
    delay: 150,
    interval: 150,
    distance: '300px',
    origin: 'bottom',
  });

ScrollReveal().reveal(
  '.card-1 .card, .card-2 .card, .card-3 .card, .card-4 .card, .multiple-button button, .form-div, .resume-1-subpart, .two-column-resume .right-block h3, .two-column-resume .right-block p', {
    delay: 150,
    interval: 150,
    distance: '300px',
    origin: 'right',
  });


ScrollReveal().reveal(
  '.contact-text', {
    delay: 150,
    interval: 150,
    distance: '300px',
    origin: 'left',
  });



//   $('.dropdown').on('show.bs.dropdown', function(e){
//     $(this).find('.dropdown-menu').removeClass('lightSpeedOut').addClass('lightSpeedIn').show();
// });

// $('.dropdown').on('hide.bs.dropdown', function(e){
//     $(this).find('.dropdown-menu').removeClass('lightSpeedIn').addClass('lightSpeedOut');
// });